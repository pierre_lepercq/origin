﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro_Mngr : MonoBehaviour
{
    public string next_scene;

    void Update()
    {
        //  if any key pressed
        if (Input.anyKey)
        {
            //  load "next_scene"
            //  it must be defined in "Build Settings" before 
            SceneManager.LoadScene("Scenes/" + next_scene);
        }
    }
}
